'use strict'

const graphql = require('graphql');
const { ScoreType } = require('./types');
const resolvers = require('./resolvers');

const eventScoresByCpf = {
  type: new graphql.GraphQLList(ScoreType),
  description: 'List events score by cpf',
  args: {
    cpf: {
      name: 'CPF. Only numbers',
      type: new graphql.GraphQLNonNull(graphql.GraphQLString)
    },
    limit: {
      name: 'Limit results',
      type: new graphql.GraphQLNonNull(graphql.GraphQLInt)
    },
  },
  resolve: resolvers.eventScoresByCpf
}

const lastEventScoreByCpf = {
  type: ScoreType,
  description: 'Get event score by CPF',
  args: {
    cpf: {
      name: 'CPF. Only numbers',
      type: new graphql.GraphQLNonNull(graphql.GraphQLString)
    }
  },
  resolve: resolvers.lastEventScoreByCpf
}

module.exports = {
  eventScoresByCpf: eventScoresByCpf,
  lastEventScoreByCpf: lastEventScoreByCpf
}
