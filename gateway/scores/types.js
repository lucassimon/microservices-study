'use strict'

let graphql = require('graphql');

const scoreType = new graphql.GraphQLObjectType({
  name: 'Score',
  description: 'Score',
  fields: {
    id: {
      type: new graphql.GraphQLNonNull(graphql.GraphQLID),
      description: 'Refer a uuid primary key',
      resolve(root, args, info) {
        return root._id
      }
    },
    name: {
      type: graphql.GraphQLString,
      description: 'The name of person',
    },
    score: {
      type: graphql.GraphQLInt,
      description: 'The score',
    },
    person_id: {
      type: graphql.GraphQLString,
      description: 'The person id',
    },
    user_id: {
      type: graphql.GraphQLString,
      description: 'The user id',
    },
    created: {
      type: graphql.GraphQLString,
      description: 'Created At',
    }
  }
})

module.exports = {
  ScoreType: scoreType
}