'use strict'

const rp = require('request-promise');

exports.eventScoresByCpf = async (root, params) => {
  return [{
    "id": "1",
    "name": "Lucas simon Rodrigues Magalhaes",
    "score": 77
  },{
    "id": "2",
    "name": "Saint Martin",
    "score": 888
  }]
}

exports.lastEventScoreByCpf = async (root, params, context) => {
  const options = {
    method: 'POST',
    uri: 'http://localhost:3000',
    body: {
      cpf: params.cpf
    },
    json: true // Automatically stringifies the body to JSON
  };
  const event = rp(options)

  return event
}
