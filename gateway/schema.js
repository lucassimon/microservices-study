'use strict'

const graphql = require('graphql');
const { persons, personByCpf } = require('./persons/queries');
const { eventScoresByCpf, lastEventScoreByCpf } = require('./scores/queries');
const { listDebts } = require('./debts/queries');
const { financialMovement } = require('./goods/queries');
const { lastTransactionCreditCard } = require('./transactions/queries');

const schema = new graphql.GraphQLSchema({
  query: new graphql.GraphQLObjectType({
    name: 'RootQuery',
    fields: {
      persons: persons,
      person: personByCpf,
      eventScoresByCpf: eventScoresByCpf,
      lastEventScoreByCpf: lastEventScoreByCpf,
      listDebts: listDebts,
      financialMovement: financialMovement,
      lastTransactionCreditCard: lastTransactionCreditCard
    }
  }),
})

module.exports = schema