'use strict'

const Hapi = require('hapi');
const Good = require('good');

// GraphQL

const { graphqlHapi, graphiqlHapi } = require('apollo-server-hapi');
const myGraphQLSchema = require('./schema');

// Create a server with a host and port
const server = Hapi.server({
  host:'localhost', port:7000,
  routes: {
    cors: {
      origin: ['*']
    }
  }
});


// Start the server
async function start() {

  try {
    await server.register([
      {
        plugin: graphqlHapi,
        options: {
          path: '/graphql',
          graphqlOptions: {
            schema: myGraphQLSchema,
          },
          route: {
            cors: true
          }
        },
      },
      {
        plugin: graphiqlHapi,
        options: {
          path: '/graphiql',
          graphiqlOptions: {
            endpointURL: '/graphql',
          },
        },
      }
    ]);
    await server.start();
  }
  catch (err) {
    process.exit(1);
  }

  console.log('Server running at:', server.info.uri);
};

start();