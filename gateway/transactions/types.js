'use strict'

let graphql = require('graphql');

const transactionType = new graphql.GraphQLObjectType({
  name: 'Transactions',
  description: 'Transactions',
  fields: {
    id: {
      type: new graphql.GraphQLNonNull(graphql.GraphQLID),
      description: 'Refer a uuid primary key'
    },
    name: {
      type: graphql.GraphQLString,
      description: 'The name of person',
    },
  }
})

module.exports = {
  TransactionType: transactionType
}