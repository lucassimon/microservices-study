'use strict'

const graphql = require('graphql');
const { TransactionType } = require('./types');
const resolvers = require('./resolvers');

const lastTransactionCreditCard = {
  type: TransactionType,
  description: 'Last transaction by cpf',
  args: {
    cpf: {
      name: 'CPF. Only numbers',
      type: new graphql.GraphQLNonNull(graphql.GraphQLString)
    }
  },
  resolve: resolvers.lastTransactionCreditCard
}

module.exports = {
  lastTransactionCreditCard: lastTransactionCreditCard
}
