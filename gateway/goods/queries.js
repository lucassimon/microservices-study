'use strict'

const graphql = require('graphql');
const { GoodType } = require('./types');
const resolvers = require('./resolvers');

const financialMovement = {
  type: GoodType,
  description: 'Financial movement by cpf',
  args: {
    cpf: {
      name: 'CPF. Only numbers',
      type: new graphql.GraphQLNonNull(graphql.GraphQLString)
    }
  },
  resolve: resolvers.financialMovement
}

module.exports = {
  financialMovement: financialMovement
}
