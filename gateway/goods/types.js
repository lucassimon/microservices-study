'use strict'

let graphql = require('graphql');

const goodType = new graphql.GraphQLObjectType({
  name: 'Goods',
  description: 'Goods',
  fields: {
    id: {
      type: new graphql.GraphQLNonNull(graphql.GraphQLID),
      description: 'Refer a uuid primary key'
    },
    name: {
      type: graphql.GraphQLString,
      description: 'The name of person',
    },
  }
})

module.exports = {
  GoodType: goodType
}