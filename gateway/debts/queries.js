'use strict'

const graphql = require('graphql');
const { DebtType } = require('./types');
const resolvers = require('./resolvers');

const listDebts = {
  type: new graphql.GraphQLList(DebtType),
  description: 'List debts by cpf',
  args: {
    cpf: {
      name: 'CPF. Only numbers',
      type: new graphql.GraphQLNonNull(graphql.GraphQLString)
    }
  },
  resolve: resolvers.listDebts
}

module.exports = {
  listDebts: listDebts
}
