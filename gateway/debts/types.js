'use strict'

let graphql = require('graphql');

const debtType = new graphql.GraphQLObjectType({
  name: 'Debts',
  description: 'Debts',
  fields: {
    id: {
      type: new graphql.GraphQLNonNull(graphql.GraphQLID),
      description: 'Refer a uuid primary key'
    },
    name: {
      type: graphql.GraphQLString,
      description: 'The name of person',
    },
  }
})

module.exports = {
  DebtType: debtType
}