'use strict'

const graphql = require('graphql');
const { PersonType } = require('./types');
const resolvers = require('./resolvers');

const persons = {
  type: new graphql.GraphQLList(PersonType),
  description: 'List persons',
  resolve: resolvers.all
}

const personByCpf = {
  type: PersonType,
  description: 'Get person by CPF',
  args: {
    cpf: {
      name: 'CPF. Only numbers',
      type: new graphql.GraphQLNonNull(graphql.GraphQLString)
    }
  },
  resolve: resolvers.find_by_cpf
}

module.exports = {
  persons: persons,
  personByCpf: personByCpf
}
