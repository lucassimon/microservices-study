'use strict'

let graphql = require('graphql');

const personType = new graphql.GraphQLObjectType({
  name: 'Persons',
  description: 'Persons',
  fields: {
    id: {
      type: new graphql.GraphQLNonNull(graphql.GraphQLID),
      description: 'Refer a uuid primary key'
    },
    name: {
      type: graphql.GraphQLString,
      description: 'The name of person',
    },
  }
})

module.exports = {
  PersonType: personType
}