# -*- coding:utf-8 -*-

# Python


# Flask
from flask import request

# Third
from flask_jwt_extended import jwt_optional
from flask_restful import Resource

# Apps
from apps.messages import _MSG205
from apps.responses import resp_exception

# Local
from .models import Income
from .schemas import IncomeSchema


class IncomesByCpf(Resource):

    @jwt_optional
    def get(self, cpf, page_id):
        # TODO: Get user identity if it is logged in
        schema = IncomeSchema(many=True)
        criteria = {}

        if page_id < 1:
            page_id = 1

        if 'page_size' in request.args:
            if int(request.args.get('page_size')) < 1:
                page_size = 10
            else:
                page_size = int(request.args.get('page_size'))

        else:
            page_size = 10

        criteria = {
            'cpf': cpf,
        }

        try:
            result = Income.objects(**criteria).paginate(
                page_id, page_size
            )

        except Exception as e:
            return resp_exception('Income', description=e)

        items = schema.dump(result.items)

        return {
            'status': 200, 'resource': 'Incomes',
            'message': _MSG205.format('Income'), 'data': items.data,
            'page': result.page, 'pages': result.pages, 'total': result.total
        }, 200
