# -*- coding:utf-8 -*-


# Third
from marshmallow import Schema
from marshmallow.fields import Date, Integer, Str

# Apps
from apps.messages import _MSG309


class IncomeSchema(Schema):
    id = Str()
    cpf = Str(required=True, error_messages={'required': _MSG309})
    name = Str(required=True, error_messages={'required': _MSG309})
    person_id = Str(required=True, error_messages={'required': _MSG309})
    value = Integer(required=True, error_messages={'required': _MSG309})
    created = Date()
