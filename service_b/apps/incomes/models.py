# -*- coding: utf-8 -*-

# Python
from datetime import datetime

# Third
from mongoengine import (
    StringField,
    IntField,
    ObjectIdField,
    DateTimeField
)

# Apps
from apps.db import db


class Income(db.Document):
    meta = {'collection': 'incomes'}

    person_id = ObjectIdField(required=True)
    cpf = StringField(required=True)
    name = StringField(required=True)
    value = IntField(required=True)
    created = DateTimeField(default=datetime.now)
