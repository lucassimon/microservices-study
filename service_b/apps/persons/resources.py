# -*- coding:utf-8 -*-

# Python


# Flask


# Third

from flask_restful import Resource
from mongoengine.errors import DoesNotExist

# Apps
from apps.messages import _MSG200

from apps.responses import (
    resp_does_not_exist,
    resp_exception,
    resp_ok
)

# Local
from .models import Person
from .schemas import PersonSchema


class PersonResource(Resource):

    def get(self, cpf):
        schema = PersonSchema()

        criteria = {'cpf': cpf}

        try:
            person = Person.objects(**criteria).get()

        except DoesNotExist:
            return resp_does_not_exist('Persons', 'person')

        except Exception as e:
            return resp_exception('Persons', description=e)

        result = schema.dump(person)

        return resp_ok(
            'Persons', _MSG200.format('Person'), data=result.data
        )
