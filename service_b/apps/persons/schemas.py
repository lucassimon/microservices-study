# -*- coding:utf-8 -*-


# Third
from marshmallow import Schema
from marshmallow.fields import Boolean, Date, Integer, Method, Str

# Apps
from apps.messages import _MSG309


class PersonSchema(Schema):
    id = Str()
    cpf = Str(required=True, error_messages={'required': _MSG309})
    name = Str(required=True, error_messages={'required': _MSG309})
    address = Str(required=True, error_messages={'required': _MSG309})
    birthdate = Date(required=True, error_messages={'required': _MSG309})
    score = Integer(required=True, error_messages={'required': _MSG309})
    is_criminal = Boolean(required=True, error_messages={'required': _MSG309})
    has_debts = Boolean(required=True, error_messages={'required': _MSG309})
    has_clonned_credit_card = Boolean(
        required=True, error_messages={'required': _MSG309}
    )
    age = Method("get_age")
    search_at = Date()
    created = Date()

    def get_age(self, obj):
        return obj.age()
