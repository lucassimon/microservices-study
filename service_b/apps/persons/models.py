# -*- coding: utf-8 -*-

# Python
from datetime import datetime

# Third
from mongoengine import (
    BooleanField,
    DateTimeField,
    StringField,
    IntField
)

# Apps
from apps.db import db


class Person(db.Document):
    meta = {'collection': 'persons'}

    cpf = StringField(required=True, unique=True)
    name = StringField(required=True)
    address = StringField(required=True)
    birthdate = DateTimeField(required=True)
    score = IntField(required=True)

    is_criminal = BooleanField(default=False)
    has_debts = BooleanField(default=False)
    has_clonned_credit_card = BooleanField(default=False)

    search_at = DateTimeField()
    created = DateTimeField(default=datetime.now)

    def age(self):
        return datetime.today().year - self.birthdate.year
