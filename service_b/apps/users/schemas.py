# -*- coding:utf-8 -*-


# Third
from marshmallow import Schema
from marshmallow.fields import Boolean, Email, Str

# Apps
from apps.messages import _MSG309


class PermissionSchema(Schema):
    admin = Boolean()
    company = Boolean()
    ia = Boolean()


class UserCreateSchema(Schema):
    email = Email(required=True, error_messages={'required': _MSG309})
    password = Str(required=True, error_messages={'required': _MSG309})


class UserSchema(Schema):
    id = Str()
    email = Str(required=True, error_messages={'required': _MSG309})
    name = Str(required=True, error_messages={'required': _MSG309})


class UpdateUserSchema(Schema):
    email = Str()
    name = Str()
