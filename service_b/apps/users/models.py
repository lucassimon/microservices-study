# -*- coding: utf-8 -*-

# Python
from datetime import datetime

# Third
from mongoengine import (
    BooleanField,
    DateTimeField,
    EmailField,
    EmbeddedDocument,
    EmbeddedDocumentField,
    StringField,
)

# Apps
from apps.db import db


class Permission(EmbeddedDocument):
    admin = BooleanField(default=False)
    company = BooleanField(default=False)
    ia = BooleanField(default=False)


class UserMixin(db.Document):
    """
    Default implementation for User fields
    """
    meta = {
        'abstract': True,
        'ordering': ['email']
    }

    email = EmailField(required=True, unique=True)
    password = StringField(required=True)
    permissions = EmbeddedDocumentField(Permission, default=Permission)

    created = DateTimeField(default=datetime.now)
    active = BooleanField(default=True)

    def is_active(self):
        return self.active


class User(UserMixin):
    '''
    Users
    '''
    meta = {'collection': 'users'}
