# -*- coding: utf-8 -*-

# Python


# Third
from mongoengine.errors import DoesNotExist

# Apps

from apps.responses import resp_does_not_exist, resp_exception
from .models import User


def get_user(resource, identity=None, object_id=None):
    try:
        if object_id:
            return User.objects.get(id=object_id)
        else:
            return User.objects.get(email=identity)

    except DoesNotExist:
        return resp_does_not_exist(resource, 'usuário')

    except Exception as e:
        return resp_exception(resource, description=e)


def user_is_active(identity):
    '''
    Check in database if the user, by search email, is active

    When a JWT generated it's ok, but the active field in user database is
    false
    '''

    try:
        user = User.objects.get(email=identity)
    except DoesNotExist:
        return False

    if user and user.active:
        return True

    return False
