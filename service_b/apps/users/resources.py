# -*- coding:utf-8 -*-

# Python

# Flask
from flask import request

# Third
from flask_restful import Resource
from bcrypt import gensalt, hashpw
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_required
)
from mongoengine.errors import NotUniqueError, ValidationError


# Apps
from apps.messages import _MSG103, _MSG105
from apps.messages import _MSG200, _MSG201, _MSG202, _MSG203
from apps.messages import _MSG300, _MSG302, _MSG312

from apps.responses import resp_already_exists, resp_exception
from apps.responses import resp_form_invalid, resp_ok, resp_notallowed_user

# Local
from .models import User
from .schemas import UserCreateSchema, UserSchema, UpdateUserSchema
from .validate import exists_email_in_database
from .utils import get_user


class UsersList(Resource):
    def post(self, *args, **kwargs):
        '''
        Create an user

        payload:
        {
            "email": "lucassrod@gmail.com",
            "password": "teste123"
        }
        '''
        req_data = request.get_json() or None
        schema = UserCreateSchema()

        if req_data is None:
            return resp_form_invalid('Users', [], msg=_MSG312)

        password = req_data.get('password', None)
        confirm_password = req_data.pop('confirm_password', None)

        if password and confirm_password and not password == confirm_password:
            errors = {'password': _MSG105}

            return resp_form_invalid('Users', errors)

        data, errors = schema.load(req_data)

        if errors:
            return resp_form_invalid('Users', errors)

        if exists_email_in_database(data.get('email')):
            errors = {
                'errors': {
                    'email': [_MSG302]
                }
            }

            return resp_form_invalid('Users', errors)

        # Encrypt password with bcrypt

        hashed = hashpw(password.encode('utf-8'), gensalt(12))

        try:
            data['password'] = hashed
            data['email'] = data['email'].lower()
            model = User(**data)
            model.save()

        except NotUniqueError:
            return resp_already_exists('Users', 'usuário')

        except ValidationError as e:
            return resp_exception('Users', msg=_MSG300, description=e)

        except Exception as e:
            return resp_exception('Users', description=e)

        schema = UserSchema()
        result = schema.dump(model)

        extras = {
            'token': create_access_token(identity=model.email),
            'refresh': create_refresh_token(identity=model.email)
        }

        return resp_ok(
            'Users', _MSG200.format('Usuário'), data=result.data, **extras
        )


class UserResource(Resource):

    @jwt_required
    def get(self, user_id):
        jwt_user = get_jwt_identity()

        if not jwt_user:
            return resp_notallowed_user('Users', _MSG103)

        user = get_user('Users', identity=jwt_user)

        if not isinstance(user, User):
            return user

        if not user.is_active():
            return resp_notallowed_user('Auth', _MSG103)

        schema = UserSchema()
        result = schema.dump(user)

        return resp_ok(
            'Users', _MSG201.format('Usuário'), data=result.data
        )

    @jwt_required
    def put(self, user_id):
        jwt_user = get_jwt_identity()

        if not jwt_user:
            return resp_notallowed_user('Users', _MSG103)

        user = get_user('Users', identity=jwt_user)

        if not isinstance(user, User):
            return user

        if not user.is_active():
            return resp_notallowed_user('Auth', _MSG103)

        req_data = request.get_json() or None

        if req_data is None:
            return resp_form_invalid('Users', [], msg=_MSG312)

        schema = UpdateUserSchema()

        data, errors = schema.load(req_data)

        if errors:
            return resp_form_invalid('Users', errors)

        try:
            for i in data.keys():
                user[i] = data[i]

            user.save()

        except NotUniqueError as e:
            return resp_already_exists('Users', 'usuário')

        except ValidationError as e:
            return resp_exception('Users', msg=_MSG300, description=e)

        except Exception as e:
            return resp_exception('Users', description=e)

        schema = UserSchema()
        result = schema.dump(user)

        return resp_ok(
            'Users', _MSG202.format('Usuário'), data=result.data
        )

    @jwt_required
    def delete(self, user_id):
        jwt_user = get_jwt_identity()

        if not jwt_user:
            return resp_notallowed_user('Users', _MSG103)

        user = get_user('Users', identity=jwt_user)

        if not isinstance(user, User):
            return user

        if not user.is_active():
            return resp_notallowed_user('Auth', _MSG103)

        user.active = False

        try:
            user.save()
        except NotUniqueError as e:
            return resp_already_exists('Users', 'usuário')

        except ValidationError as e:
            return resp_exception(
                'Users', msg=_MSG300, description=e
            )

        except Exception as e:
            return resp_exception('Users', description=e)

        return resp_ok('Users', _MSG203.format('Usuário'))
