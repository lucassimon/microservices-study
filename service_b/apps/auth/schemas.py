# -*- coding:utf-8 -*-


# Third
from marshmallow import Schema
from marshmallow.fields import Email, Str

# Apps
from apps.messages import _MSG309


class LoginMinix(object):
    email = Email(required=True, error_messages={'required': _MSG309})
    password = Str(required=True, error_messages={'required': _MSG309})


class UserLoginSchema(Schema, LoginMinix):
    pass
