# -*- coding:utf-8 -*-

# Python

from flask import request

# Third
from bcrypt import checkpw

from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_refresh_token_required,
)
from flask_restful import Resource
# Apps

from apps.messages import (
    _MSG100,
    _MSG101,
    _MSG103,
    _MSG104,
    _MSG312
)
from apps.responses import (
    resp_form_invalid,
    resp_notallowed_user,
    resp_ok
)
from apps.users.models import User
from apps.users.schemas import UserSchema
from apps.users.utils import get_user
from apps.utils import encode

# Local
from .schemas import UserLoginSchema


class AuthResource(Resource):
    def post(self, *args, **kwargs):
        '''
        Route to do login in API
        '''
        req_data = request.get_json() or None
        model = None
        sup_schema = UserLoginSchema()

        if req_data is None:
            return resp_form_invalid('Users', [], msg=_MSG312)

        data, errors = sup_schema.load(req_data)

        if errors:
            return resp_form_invalid('Users', errors)

        model = get_user('User', identity=data.get('email'))

        if not isinstance(model, User):
            return model

        if not model.is_active():
            return resp_notallowed_user('Auth', _MSG103)

        if checkpw(encode(data.get('password')), encode(model.password)):
            extras = {
                'token': create_access_token(identity=model.email),
                'refresh': create_refresh_token(identity=model.email)
            }

            schema = UserSchema()
            result = schema.dump(model)

            return resp_ok(
                'Auth', _MSG100.format('User'), data=result.data,
                **extras
            )

        return {'status': 401, 'message': _MSG101, 'resource': 'Auth'}, 401


class RefreshTokenResource(Resource):
    method_decorators = {'post': [jwt_refresh_token_required]}

    def post(self, *args, **kwargs):
        '''
        Refresh a token that expired.

        http://flask-jwt-extended.readthedocs.io/en/latest/refresh_tokens.html
        '''
        return {
            'status': 200,
            'token': create_access_token(identity=get_jwt_identity()),
            'message': _MSG104,
            'resource': 'Auth'
        }, 200
