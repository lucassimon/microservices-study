# -*- coding: utf-8 -*-

# Python
from datetime import datetime

# Third
from mongoengine import (
    StringField,
    IntField,
    ObjectIdField,
    BooleanField,
    DateTimeField
)

# Apps
from apps.db import db


class Good(db.Document):
    meta = {'collection': 'goods'}

    person_id = ObjectIdField(required=True)
    cpf = StringField(required=True)
    name = StringField(required=True)
    value = IntField(required=True)
    selled = BooleanField(required=True, default=False)
    created = DateTimeField(default=datetime.now)
