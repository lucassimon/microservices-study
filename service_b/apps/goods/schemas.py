# -*- coding:utf-8 -*-


# Third
from marshmallow import Schema
from marshmallow.fields import Boolean, Date, Integer, Str

# Apps
from apps.messages import _MSG309


class GoodSchema(Schema):
    id = Str()
    cpf = Str(required=True, error_messages={'required': _MSG309})
    name = Str(required=True, error_messages={'required': _MSG309})
    person_id = Str(required=True, error_messages={'required': _MSG309})
    value = Integer(required=True, error_messages={'required': _MSG309})
    selled = Boolean(required=True, error_messages={'required': _MSG309})
    created = Date()
