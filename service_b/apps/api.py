# -*- coding: utf-8 -*-

# Importamos as classes API e Resource
from flask_restful import Api, Resource

# Apps
from apps.users.resources import (
    UsersList, UserResource
)
from apps.auth.resources import (
    AuthResource, RefreshTokenResource
)

from apps.persons.resources import PersonResource
from apps.incomes.resources import IncomesByCpf
from apps.goods.resources import GoodsByCpf


# Criamos uma classe que extende de Resource
class Index(Resource):

    # Definimos a operação get do protocolo http
    def get(self):

        # retornamos um simples dicionário que será automáticamente
        # retornado em json pelo flask
        return {'hello': 'world by apps'}


# Instânciamos a API do FlaskRestful
api = Api()


def configure_api(app):

    # adicionamos na rota '/' a sua classe correspondente Index
    api.add_resource(Index, '/')

    # Users
    api.add_resource(UsersList, '/users')
    api.add_resource(UserResource, '/users/<string:user_id>')

    # Auth
    api.add_resource(AuthResource, '/auth')
    api.add_resource(RefreshTokenResource, '/auth/refresh')

    # Person
    api.add_resource(PersonResource, '/persons/<string:cpf>')

    # Income
    api.add_resource(
        IncomesByCpf, '/incomes/<string:cpf>/page/<int:page_id>'
    )

    # Goods
    api.add_resource(
        GoodsByCpf, '/goods/<string:cpf>/page/<int:page_id>'
    )

    # inicializamos a api com as configurações do flask vinda por parâmetro
    api.init_app(app)
