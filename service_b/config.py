# -*- coding: utf-8 -*-

# Python
from datetime import timedelta
from os import getenv


class Config:
    SECRET_KEY = getenv('SECRET_KEY') or 'uma string randômica e gigante'
    APP_PORT = int(getenv('APP_PORT'))
    DEBUG = eval(getenv('DEBUG').title())
    MONGODB_HOST = getenv('MONGODB_URI')
    MONGODB_HOST_TEST = getenv('MONGODB_URI_TEST')
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(
        hours=int(getenv('JWT_EXPIRING_TIME'))
    )


class DevelopmentConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True


class TestingConfig(Config):
    FLASK_ENV = 'testing'
    TESTING = True
    MONGODB_HOST = getenv('MONGODB_URI_TEST')


class ProductionConfig(Config):
    DEBUG = False


class HerokuConfig(ProductionConfig):
    SSL_REDIRECT = True if getenv('DYNO') else False


class DockerConfig(ProductionConfig):
    pass


class UnixConfig(ProductionConfig):
    pass


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'heroku': HerokuConfig,
    'docker': DockerConfig,
    'unix': UnixConfig,
    'default': DevelopmentConfig
}
