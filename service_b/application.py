# -*- coding: utf-8 -*-

from os import getenv
from os.path import dirname, isfile, join


from dotenv import load_dotenv


# a partir do arquivo atual adicione ao path o arquivo .env
_ENV_FILE = join(dirname(__file__), '.env')

# existindo o arquivo faça a leitura do arquivo através da função load_dotenv
if isfile(_ENV_FILE):
    load_dotenv(dotenv_path=_ENV_FILE)


from apps import create_app

# instancia nossa função factory criada anteriormente
app = create_app(getenv('FLASK_ENV') or 'default')


@app.cli.command()
def adds_sample_data():
    '''
    add sample data
    '''
    from apps.persons.models import Person
    from apps.goods.models import Good
    from apps.incomes.models import Income
    from apps.users.models import User

    person = {
        "cpf": "07368680629",
        "name": "Lucas Simon Rodrigues Magalhaes",
        "address": "Rua Manoel Sabino Nogueira, 164, Palmares, Belo Horizonte, Minas Gerais",
        "birthdate": "1987-04-09T11:10:56Z",
        "score": 88
    }

    person = Person(**person).save()

    for i in range(1, 50):

        value = 100 * i
        income = {
            "person_id": person.id,
            "cpf": "{}".format(person.cpf),
            "name": f"Desenvolvimento. {i} horas de trabalho a R$ 1.00/h",
            "value": value
        }

        Income(**income).save()

    Good.objects.insert(
        [
            Good(
                person_id=person.id,
                cpf=person.cpf,
                name="KTM DUKE 390 17/17",
                value=2100000
            ),
            Good(
                person_id=person.id,
                cpf=person.cpf,
                name="Notebook Dell Inspirion",
                value=400000
            ),
            Good(
                person_id=person.id,
                cpf=person.cpf,
                name="Iphone Gold",
                value=1500000
            ),
            Good(
                person_id=person.id,
                cpf=person.cpf,
                name="Apto 3 Qtos, 1 garagem, 1 andar, bairro centro",
                value=10000000000
            ),

        ]
    )


if __name__ == '__main__':
    ip = '0.0.0.0'
    port = app.config['APP_PORT']
    debug = app.config['DEBUG']

    # executa o servidor web do flask
    app.run(
        host=ip, debug=debug, port=port, use_reloader=debug
    )
