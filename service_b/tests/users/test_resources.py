# -*- coding: utf-8 -*-

# Python
from json import loads, dumps
# Third

# apps
from apps.messages import _MSG300, _MSG312, _MSG309


class TestUsersList:
    def setup_method(self):
        self.data = {}
        self.ENDPOINT = '/users'

    def test_response_400_when_there_are_empty_post_data(self, client):
        resp = client.post(self.ENDPOINT)
        assert resp.status_code == 400

    def test_message_312_when_there_are_empty_post_data(self, client):
        resp = client.post(self.ENDPOINT)
        data = loads(resp.data.decode('utf-8'))
        assert data['message'] == _MSG312

    def test_response_400_when_payload_is_invalid(self, client):
        resp = client.post(
            self.ENDPOINT, data=dumps(dict(foo='bar')),
            content_type='application/json'
        )

        assert resp.status_code == 400

    def test_message_300_when_form_is_invalid(self, client):
        resp = client.post(
            self.ENDPOINT, data=dumps(dict(foo='bar')),
            content_type='application/json'
        )
        data = loads(resp.data.decode('utf-8'))
        assert data['message'] == _MSG300

    def test_show_all_errors_required_when_form_is_invalid(self, client):
        resp = client.post(
            self.ENDPOINT, data=dumps(dict(foo='bar')),
            content_type='application/json'
        )
        data = loads(resp.data.decode('utf-8'))

        require_fields = ['email', 'password']

        for field in require_fields:
            assert field in data['errors'].keys()
            assert data['errors'][field][0] == _MSG309

    def test_save_an_user(self, client, mongo):
        payload = {
            'email': 'teste1@test.com',
            'password': 'teste123'
        }

        resp = client.post(
            self.ENDPOINT, data=dumps(payload),
            content_type='application/json'
        )

        data = loads(resp.data.decode('utf-8'))

        assert resp.status_code == 200
        assert data.get('message') == 'Usuário criado(a).'
