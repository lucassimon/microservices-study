# -*- coding: utf-8 -*-

# Python
from json import dumps, loads

# Apps
# apps
from apps.messages import _MSG300, _MSG309, _MSG312
from apps.responses import resp_does_not_exist


class TestAuthResource:
    def setup_method(self):
        self.data = {}
        self.ENDPOINT = '/auth'

    def test_response_400_when_there_are_empty_post_data(self, client):
        resp = client.post(self.ENDPOINT)
        assert resp.status_code == 400

    def test_message_309_when_there_are_empty_post_data(self, client):
        resp = client.post(self.ENDPOINT)
        data = loads(resp.data.decode('utf-8'))
        assert data['message'] == _MSG312

    def test_response_400_when_form_is_invalid(self, client):
        resp = client.post(
            self.ENDPOINT, data=dumps(dict(foo='bar')),
            content_type='application/json'
        )

        assert resp.status_code == 400

    def test_message_300_when_form_is_invalid(self, client):
        resp = client.post(
            self.ENDPOINT, data=dumps(dict(foo='bar')),
            content_type='application/json'
        )
        data = loads(resp.data.decode('utf-8'))
        assert data['message'] == _MSG300

    def test_show_all_errors_required_when_form_is_invalid(self, client):
        resp = client.post(
            self.ENDPOINT, data=dumps(dict(foo='bar')),
            content_type='application/json'
        )
        data = loads(resp.data.decode('utf-8'))
        require_fields = ['email', 'password']

        for field in require_fields:
            assert field in data['errors'].keys()

    def test_message_309_when_form_is_invalid(self, client):

        resp = client.post(
            self.ENDPOINT, data=dumps(dict(foo='bar')),
            content_type='application/json'
        )

        data = loads(resp.data.decode('utf-8'), encoding='utf-8').get('errors')

        for field in data.keys():
            assert _MSG309 in data[field]

    def test_response_does_not_exist(self, client):
        payload = {'email': 'testOne@test.com', 'password': 'testOne'}

        resp = client.post(
            self.ENDPOINT, data=dumps(payload), content_type='application/json'
        )

        does_not_exist = resp_does_not_exist('User', 'usuário')

        assert resp.data == does_not_exist.data

    def test_authenticate_an_user(self, client, mongo):

        payload = {
            'email': 'teste1@test.com',
            'password': 'teste123'
        }

        resp = client.post(
            '/users', data=dumps(payload),
            content_type='application/json'
        )

        resp = client.post(
            self.ENDPOINT, data=dumps(payload), content_type='application/json'
        )

        data = loads(resp.data.decode('utf-8'))
        assert resp.status_code == 200
        assert data.get('message') == 'Token criado.'
