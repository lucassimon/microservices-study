'use strict'

const Resource = require('./resources')
const Schema = require('./validSchema')
const Joi = require('joi')

module.exports = [
  {
    method: 'GET',
    path: '/products',
    config: {
      description: 'List all products',
      tags: ['api', 'cc', 'get all products'],
      cache: {
        expiresIn: 30 * 1000,
        privacy: 'private'
      }
    },
    handler: Resource.list
  },
  {
    method: 'POST',
    path: '/products',
    config: {
      description: 'Create a products.',
      tags: ['api', 'cc', 'post products'],
      validate: {
        payload: Schema.Add
      },
      payload: {
        parse:true,
        allow: ['application/json']
      },
      auth: false
    },
    handler: Resource.create
  },
  {
    method: 'GET',
    path: '/products/{id}/',
    config: {
      description: 'Get products detail.',
      tags: ['api', 'cc', 'get products by id'],
      validate: {
        params: {
          id: Joi.number().integer()
        }
      }
    },
    handler: Resource.find_by_id
  },

]