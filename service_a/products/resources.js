'use strict'

const Product = require('./models');

exports.list = async (req, h) => {
  let result = null

  try {
    result =  await Product.fetchAll({
      columns:['id', 'name',]
    })
  } catch (error) {

    return h.response({
      'success': false, 'message': 'Something went wrong on fetch products',
    })
  }

  return h.response({
    'success': true,
    'message': 'Fetch all products',
    'data': result
  }).code(200)
}


exports.create = async (req, h) => {
  let instance = null
  let data = {}

  try {
    instance = await Product.forge(req.payload).save();

  } catch (error) {
    return h.response({
      'success': false, 'message': 'Something went wrong when created  a product.'
    })
  }

  data['name'] = instance.attributes.name

  return h.response({
    'success': true, 'message': 'Product created.', 'data': data
  })
}


exports.find_by_id = async (req, h) => {
  let id = null, instance = null, data = {};

  if (req.params.id) {
    id = encodeURIComponent(req.params.id)
  }

  try {
    instance = await Product.forge({id: id}).fetch({require: true})

  } catch (error) {

    return h.response({
      'success': false, 'message': 'Failed to get the product'
    })
  }

  data = {
    id: instance.attributes.id,
    name: instance.attributes.name,
  }

  return h.response({
    'success': true,
    'message': 'Fetch the product.',
    'data': data
  }).code(200)
}