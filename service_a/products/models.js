'use strict'

let db = require('../settings/db')

let Product = db.Model.extend({
  tableName: 'product',
  hasTimestamps: true
})

module.exports = Product
