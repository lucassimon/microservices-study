'use strict'

const Resource = require('./resources')
const Schema = require('./validSchema')
const Joi = require('joi')

module.exports = [
  {
    method: 'GET',
    path: '/persons',
    config: {
      description: 'List all persons',
      tags: ['api', 'cc', 'get all persons'],
      cache: {
        expiresIn: 30 * 1000,
        privacy: 'private'
      }
    },
    handler: Resource.list
  },
  {
    method: 'POST',
    path: '/persons',
    config: {
      description: 'Create a person.',
      tags: ['api', 'cc', 'post person'],
      validate: {
        payload: Schema.Add
      },
      payload: {
        parse:true,
        allow: ['application/json']
      },
      auth: false
    },
    handler: Resource.create
  },
  {
    method: 'GET',
    path: '/persons/{cpf}',
    config: {
      description: 'Get person detail.',
      tags: ['api', 'cc', 'get person by cpf'],
      validate: {
        params: {
          cpf: Joi.string()
        }
      }
    },
    handler: Resource.find_by_cpf
  },

]