'use strict'

let db = require('../settings/db')

let Person = db.Model.extend({
  tableName: 'person',
  hasTimestamps: true
})

module.exports = Person
