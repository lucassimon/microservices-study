'use strict'

const Person = require('./models');

exports.list = async (req, h) => {
  let result = null

  try {
    result =  await Person.fetchAll({
      columns:['id', 'cpf', 'name', 'address']
    })
  } catch (error) {

    return h.response({
      'success': false, 'message': 'Something went wrong on fetch persons',
    })
  }

  return h.response({
    'success': true,
    'message': 'Fetch all persons',
    'data': result
  }).code(200)
}


exports.create = async (req, h) => {
  let instance = null;
  let data  = {}

  try {
    instance = await Person.forge(req.payload).save();

  } catch (error) {
    console.log(error)
    return h.response({
      'success': false, 'message': 'Something went wrong when created  a person.'
    })
  }

  data = {
    cpf: instance.attributes.cpf,
    name: instance.attributes.name,
    address: instance.attributes.address
  }

  return h.response({
    'success': true, 'message': 'Person created.', 'data': data
  })
}


exports.find_by_cpf = async (req, h) => {
  let id = null, instance = null, data = {}, cpf='';


  if (req.params.cpf) {
    cpf = encodeURIComponent(req.params.cpf)
  } else {
    return h.response({
      'success': false, 'message': 'We not receive a cpf number.'
    })
  }

  try {
    instance = await Person.forge({cpf: cpf}).fetch({require: true})

  } catch (error) {

    return h.response({
      'success': false, 'message': 'Failed to get the person'
    })
  }

  data = {
    id: instance.attributes.id,
    name: instance.attributes.name,
    address: instance.attributes.address,
    cpf: instance.attributes.cpf
  }

  return h.response({
    'success': true,
    'message': 'Get the person.',
    'data': data
  }).code(200)
}