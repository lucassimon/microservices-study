'use strict'

const Joi = require('joi')

let Add = Joi.object({
  cpf: Joi.string(),
  name: Joi.string().required(),
  address: Joi.string().required()
})

let Update = Joi.object({
  name: Joi.string(),
  address: Joi.string()
})

module.exports = {
  Add: Add,
  Update: Update
}
