'use strict';

module.exports = [
  {
    method: 'GET',
    path: '/',
    config: {
        description: 'Acesso a raiz da api',
        tags: ['api', 'home',],
        cache: {
          expiresIn: 30 * 1000,
          privacy: 'private'
        }
    },
    handler: function (request, h) {
      return h.response({
        'success': true, 'message': 'Hello World'
      })
    }
  }
]
