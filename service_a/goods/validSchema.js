'use strict'

const Joi = require('joi')

let Add = Joi.object({
  name: Joi.string().required(),
  person_id: Joi.number().integer().required(),
  product_id: Joi.number().integer().required()
})


module.exports = {
  Add: Add
}
