'use strict'

const Good = require('./models');

exports.list = async (req, h) => {
  let result = null

  try {
    result =  await Good.fetchAll({
      columns:['id', 'name',]
    })
  } catch (error) {

    return h.response({
      'success': false, 'message': 'Something went wrong on fetch goods',
    })
  }

  return h.response({
    'success': true,
    'message': 'Fetch all goods',
    'data': result
  }).code(200)
}


exports.create = async (req, h) => {
  let instance = null
  let data = {}

  try {
    instance = await Good.forge(req.payload).save();

  } catch (error) {
    return h.response({
      'success': false, 'message': 'Something went wrong when created  a good.'
    })
  }

  data['name'] = instance.attributes.name

  return h.response({
    'success': true, 'message': 'Good created.', 'data': data
  })
}


exports.find_by_id = async (req, h) => {
  let id = null, instance = null, data = {};

  if (req.params.id) {
    id = encodeURIComponent(req.params.id)
  }

  try {
    instance = await Good.forge({id: id}).fetch({require: true})

  } catch (error) {

    return h.response({
      'success': false, 'message': 'Failed to get the good'
    })
  }

  data = {
    id: instance.attributes.id,
    name: instance.attributes.name,
  }

  return h.response({
    'success': true,
    'message': 'Fetch the good.',
    'data': data
  }).code(200)
}