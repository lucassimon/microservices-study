'use strict'

let db = require('../settings/db')

let Good = db.Model.extend({
  tableName: 'good',
  hasTimestamps: true
})

module.exports = Good
