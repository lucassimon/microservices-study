'use strict'

const Resource = require('./resources')
const Schema = require('./validSchema')
const Joi = require('joi')

module.exports = [
  {
    method: 'GET',
    path: '/goods',
    config: {
      description: 'List all goods',
      tags: ['api', 'cc', 'get all goods'],
      cache: {
        expiresIn: 30 * 1000,
        privacy: 'private'
      }
    },
    handler: Resource.list
  },
  {
    method: 'POST',
    path: '/goods',
    config: {
      description: 'Create a goods.',
      tags: ['api', 'cc', 'post goods'],
      validate: {
        payload: Schema.Add
      },
      payload: {
        parse:true,
        allow: ['application/json']
      },
      auth: false
    },
    handler: Resource.create
  },
  {
    method: 'GET',
    path: '/goods/{id}/',
    config: {
      description: 'Get goods detail.',
      tags: ['api', 'cc', 'get goods by id'],
      validate: {
        params: {
          id: Joi.number().integer()
        }
      }
    },
    handler: Resource.find_by_id
  },

]