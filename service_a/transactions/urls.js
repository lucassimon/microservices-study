'use strict'

const Resource = require('./resources')
const Schema = require('./validSchema')
const Joi = require('joi')

module.exports = [
  {
    method: 'POST',
    path: '/transactions',
    config: {
      description: 'Create a transactions.',
      tags: ['api', 'cc', 'post transactions'],
      validate: {
        payload: Schema.Add
      },
      payload: {
        parse:true,
        allow: ['application/json']
      },
      auth: false
    },
    handler: Resource.create
  },
  {
    method: 'GET',
    path: '/transactions/{id}/',
    config: {
      description: 'Get transactions detail.',
      tags: ['api', 'cc', 'get transactions by id'],
      validate: {
        params: {
          id: Joi.number().integer()
        }
      }
    },
    handler: Resource.find_by_id
  },
  {
    method: 'GET',
    path: '/transactions/person/{id}',
    config: {
      description: 'List all transactions',
      tags: ['api', 'cc', 'get all transactions'],
      cache: {
        expiresIn: 30 * 1000,
        privacy: 'private'
      }
    },
    handler: Resource.list_by_person
  },

]