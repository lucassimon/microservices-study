'use strict'

const Transaction = require('./models');

exports.create = async (req, h) => {
  let instance = null
  let data = {}

  try {
    instance = await Transaction.forge(req.payload).save();

  } catch (error) {
    return h.response({
      'success': false, 'message': 'Something went wrong when created  a transaction.'
    })
  }

  // TODO:
  //
  // Sends a message to Queque system when the data has been saved.
  // Do it in async way. It is not necessary wait the successful request
  // to returns a JSON to the client

  data['name'] = instance.attributes.name

  return h.response({
    'success': true, 'message': 'Transaction created.', 'data': data
  })
}


exports.find_by_id = async (req, h) => {
  let id = null, instance = null, data = {};

  if (req.params.id) {
    id = encodeURIComponent(req.params.id)
  }

  try {
    instance = await Transaction.forge({id: id}).fetch({require: true})

  } catch (error) {

    return h.response({
      'success': false, 'message': 'Failed to get the transaction'
    })
  }

  data = {
    id: instance.attributes.id,
    name: instance.attributes.name,
    person_id: instance.attributes.person_id,
    value: instance.attributes.value,
    total: instance.attributes.total,
    payment_form: instance.attributes.payment_form,
    payment_installment: instance.attributes.payment_installment
  }

  return h.response({
    'success': true,
    'message': 'Fetch the transaction.',
    'data': data
  }).code(200)
}


exports.list_by_person = async (req, h) => {
  let id = null, instance = null, data = {};

  if (req.params.id) {
    id = encodeURIComponent(req.params.id)
  }

  try {
    instance = await Transaction.forge({person_id: id}).fetch({require: true})

  } catch (error) {

    return h.response({
      'success': false, 'message': 'Failed to get the transaction'
    })
  }

  data = {
    id: instance.attributes.id,
    name: instance.attributes.name,
    person_id: instance.attributes.person_id,
    value: instance.attributes.value,
    total: instance.attributes.total,
    payment_form: instance.attributes.payment_form,
    payment_installment: instance.attributes.payment_installment
  }

  return h.response({
    'success': true,
    'message': 'Fetch the transaction.',
    'data': data
  }).code(200)
}