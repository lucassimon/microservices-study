'use strict'

let db = require('../settings/db')

let Transaction = db.Model.extend({
  tableName: 'transaction',
  hasTimestamps: true
})

module.exports = Transaction
