'use strict'

const Joi = require('joi')

let Add = Joi.object({
  name: Joi.string().required(),
  person_id: Joi.number().integer().required(),
  value: Joi.number().integer().required(),
  total: Joi.number().integer().required(),
  payment_form: Joi.string().required(),
  payment_installment: Joi.number().integer().required()
})


module.exports = {
  Add: Add
}
