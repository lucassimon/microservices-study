exports.up = knex => knex.schema.createTable(
  'good',
  table => {
    table.increments('id');
    table.biginteger('person_id').unsigned().notNullable().references('id').inTable('person').onDelete('CASCADE').index();
    table.biginteger('product_id').unsigned().notNullable().references('id').inTable('product').onDelete('CASCADE').index();
    table.string('name', 255).notNullable()
    table.boolean('selled').notNullable().defaultTo(false);
    // table.boolean('selled_who').notNullable().defaultTo(false);
    table.timestamps()
  }
)

exports.down = knex => knex.schema.dropTable('good')
