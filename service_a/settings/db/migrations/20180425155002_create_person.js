exports.up = knex => knex.schema.createTable(
  'person',
  table => {
    table.increments('id');
    table.string('cpf', 255).unique().notNullable()
    table.string('name', 255).notNullable()
    table.string('address', 255).unique().notNullable()
    table.timestamps()
  }
)

exports.down = knex => knex.schema.dropTable('person')
