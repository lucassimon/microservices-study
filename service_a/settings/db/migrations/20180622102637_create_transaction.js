exports.up = knex => knex.schema.createTable(
  'transaction',
  table => {
    table.increments('id');
    table.biginteger('person_id').unsigned().notNullable().references('id').inTable('person').onDelete('CASCADE').index();
    table.string('name', 255).notNullable()
    table.integer('value').notNullable()
    table.integer('total').notNullable()
    table.string('payment_form', 255).notNullable()
    table.integer('payment_installment').notNullable()
    table.timestamps()
  }
)

exports.down = knex => knex.schema.dropTable('transaction')
