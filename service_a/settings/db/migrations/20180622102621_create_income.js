exports.up = knex => knex.schema.createTable(
  'income',
  table => {
    table.increments('id');
    table.biginteger('person_id').unsigned().notNullable().references('id').inTable('person').onDelete('CASCADE').index();
    table.string('name', 255).notNullable()
    table.integer('value').notNullable()
    table.timestamps()
  }
)

exports.down = knex => knex.schema.dropTable('income')
