exports.up = knex => knex.schema.createTable(
  'product',
  table => {
    table.increments('id');
    table.string('name', 255).notNullable()
    table.timestamps()
  }
)

exports.down = knex => knex.schema.dropTable('product')
