exports.up = knex => knex.schema.createTable(
  'debt',
  table => {
    table.increments('id');
    table.biginteger('person_id').unsigned().notNullable().references('id').inTable('person').onDelete('CASCADE').index();
    table.string('name', 255).notNullable()
    table.integer('value').notNullable()
    table.boolean('paid').notNullable().defaultTo(false);
    table.timestamp('paided_at')
    table.timestamps()
  }
)

exports.down = knex => knex.schema.dropTable('debt')
