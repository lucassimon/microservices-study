// Update with your config settings.

module.exports = {
  dev: {
    client: 'pg',
    connection: {
      host: '127.0.0.1',
      user: 'postgres',
      password: 'postgres',
      database: 'humans',
      charset: 'utf8'
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
  prod: {
    client: 'pg',
    connection: {
      host: '127.0.0.1',
      user: 'postgres',
      password: 'postgres',
      database: 'humans',
      charset: 'utf8'
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }
}
