'use strict'

let knex = require('knex')(require('./knexfile')[process.env.NODE_ENV || 'dev'])

let bookshelf = require('bookshelf')(knex)

module.exports = bookshelf
