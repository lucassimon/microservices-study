'use strict';

const Hapi = require('hapi');
const routes = require('./urls');

// Create a server with a host and port
const server = Hapi.server({
  host:'localhost', port:8000,
  routes: {
    cors: {
      origin: ['*']
    }
  }
});

// Start the server
async function start() {

  try {
    server.route(routes);
    await server.start();
  }
  catch (err) {
    console.log(err);
    process.exit(1);
  }

  console.log('Server running at:', server.info.uri);
};

start();