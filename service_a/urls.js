const core = require('./core/urls');
const products = require('./products/urls');
const persons = require('./persons/urls');
const goods = require('./goods/urls');
const transactions = require('./transactions/urls');


module.exports = [].concat(core, products, persons, goods, transactions);
