const amqp = require('amqplib')
const promise = require('bluebird')
const config = require('./config')
const db = require('./db')
const { EventScore } = require('./model')

const assertQueueOptions = { durable: true }
const consumeQueueOptions = { noAck: false }
const { uri, workQueue } = config


const doWork = async (msg) => {
  const body = msg.content.toString()
  const data = JSON.parse(body)
  console.log(data.context);

  let event = new EventScore({
    origin: data.origin,
    published_at: data.published_at,
    person_id: data.context.person_id,
    user_id: data.context.user_id,
    name: data.context.name,
    cpf: data.context.cpf,
    score: data.context.score
  })

  await event.save()

  // throw new Error('Some error');

  console.log(" [x] Received");

}

const assertAndConsumeQueue = (channel) => {
  const ackMsg = msg => promise.resolve(
    doWork(msg)
  ).then(() => channel.ack(msg))
  .catch((error) => {
    console.log(error)
    channel.nack()
    channel.reject(msg, true)
  })

  return channel.assertQueue(workQueue, assertQueueOptions)
    .then(() => channel.prefetch(1))
    .then(() => channel.consume(workQueue, ackMsg, consumeQueueOptions));
}

const getQueueMessages = (
  () => amqp.connect(uri).then(
    connection => connection.createChannel()
  ).then(
    channel => assertAndConsumeQueue(channel)
  )
)

getQueueMessages()