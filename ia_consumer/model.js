const Mongoose = require('mongoose');


const EventScore = new Mongoose.Schema({
  origin: {type: String, required: true, trim: true},
  published_at: {type: String, required: true, trim: true},
  person_id: {type: String, required: true, trim: true},
  user_id: {type: String, required: true, trim: true},
  name: {type: String, required: true, trim: true},
  cpf: {type: String, required: true, trim: true},
  score: {type: Number, required: true}
},
{
  timestamps: true
})

module.exports = {
  EventScore: Mongoose.model('events-score', EventScore),
}
