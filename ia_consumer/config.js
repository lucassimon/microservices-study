const rabbitConfig = {
  uri: process.env.AMQP_URI ||  "amqp://root:root@localhost",
  workQueue: process.env.workQueue || "microservices.IA-123"
}

module.exports = rabbitConfig