'use strict';

const { json, send } = require('micro');
const EventScore = require('./model');
const db = require('./db');

module.exports = async function (req, res) {
  const payload = await json(req);

  const event = await EventScore.findOne({cpf:payload.cpf}).sort({createdAt: -1}).exec();

  send(res, 200, event);

}
