--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4 (Debian 10.4-2.pgdg90+1)
-- Dumped by pg_dump version 10.4 (Ubuntu 10.4-0ubuntu0.18.04)

-- Started on 2018-06-22 12:08:07 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE humans;
--
-- TOC entry 2945 (class 1262 OID 16384)
-- Name: humans; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE humans WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE humans OWNER TO postgres;

\connect humans

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12980)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 202 (class 1259 OID 16850)
-- Name: debt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.debt (
    id integer NOT NULL,
    person_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    value integer NOT NULL,
    paid boolean DEFAULT false NOT NULL,
    paided_at timestamp with time zone,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.debt OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16848)
-- Name: debt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.debt_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.debt_id_seq OWNER TO postgres;

--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 201
-- Name: debt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.debt_id_seq OWNED BY public.debt.id;


--
-- TOC entry 210 (class 1259 OID 16904)
-- Name: good; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.good (
    id integer NOT NULL,
    person_id bigint NOT NULL,
    product_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    selled boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.good OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16902)
-- Name: good_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.good_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.good_id_seq OWNER TO postgres;

--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 209
-- Name: good_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.good_id_seq OWNED BY public.good.id;


--
-- TOC entry 204 (class 1259 OID 16865)
-- Name: income; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.income (
    id integer NOT NULL,
    person_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    value integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.income OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16863)
-- Name: income_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.income_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.income_id_seq OWNER TO postgres;

--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 203
-- Name: income_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.income_id_seq OWNED BY public.income.id;


--
-- TOC entry 197 (class 1259 OID 16824)
-- Name: knex_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.knex_migrations (
    id integer NOT NULL,
    name character varying(255),
    batch integer,
    migration_time timestamp with time zone
);


ALTER TABLE public.knex_migrations OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16822)
-- Name: knex_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.knex_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.knex_migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 196
-- Name: knex_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.knex_migrations_id_seq OWNED BY public.knex_migrations.id;


--
-- TOC entry 198 (class 1259 OID 16830)
-- Name: knex_migrations_lock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.knex_migrations_lock (
    is_locked integer
);


ALTER TABLE public.knex_migrations_lock OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16835)
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person (
    id integer NOT NULL,
    cpf character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    address character varying(255) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.person OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16833)
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_id_seq OWNER TO postgres;

--
-- TOC entry 2953 (class 0 OID 0)
-- Dependencies: 199
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- TOC entry 206 (class 1259 OID 16879)
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.product OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16877)
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO postgres;

--
-- TOC entry 2954 (class 0 OID 0)
-- Dependencies: 205
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- TOC entry 208 (class 1259 OID 16887)
-- Name: transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transaction (
    id integer NOT NULL,
    person_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    value integer NOT NULL,
    total integer NOT NULL,
    payment_form character varying(255) NOT NULL,
    payment_installment integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.transaction OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16885)
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transaction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaction_id_seq OWNER TO postgres;

--
-- TOC entry 2955 (class 0 OID 0)
-- Dependencies: 207
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transaction_id_seq OWNED BY public.transaction.id;


--
-- TOC entry 2769 (class 2604 OID 16853)
-- Name: debt id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.debt ALTER COLUMN id SET DEFAULT nextval('public.debt_id_seq'::regclass);


--
-- TOC entry 2774 (class 2604 OID 16907)
-- Name: good id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.good ALTER COLUMN id SET DEFAULT nextval('public.good_id_seq'::regclass);


--
-- TOC entry 2771 (class 2604 OID 16868)
-- Name: income id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.income ALTER COLUMN id SET DEFAULT nextval('public.income_id_seq'::regclass);


--
-- TOC entry 2767 (class 2604 OID 16827)
-- Name: knex_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations ALTER COLUMN id SET DEFAULT nextval('public.knex_migrations_id_seq'::regclass);


--
-- TOC entry 2768 (class 2604 OID 16838)
-- Name: person id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- TOC entry 2772 (class 2604 OID 16882)
-- Name: product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- TOC entry 2773 (class 2604 OID 16890)
-- Name: transaction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaction ALTER COLUMN id SET DEFAULT nextval('public.transaction_id_seq'::regclass);


--
-- TOC entry 2931 (class 0 OID 16850)
-- Dependencies: 202
-- Data for Name: debt; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2939 (class 0 OID 16904)
-- Dependencies: 210
-- Data for Name: good; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.good (id, person_id, product_id, name, selled, created_at, updated_at) VALUES (1, 1, 1, 'Automóvel pela conscessionaria Yamaha, NFE: 132456798-987841251-54635458', false, '2018-06-22 14:44:06.554+00', '2018-06-22 14:44:06.554+00');
INSERT INTO public.good (id, person_id, product_id, name, selled, created_at, updated_at) VALUES (2, 1, 4, 'Eletroeletronico pela DELL, NFE: 134534-55464-56567798-987841251-54635458', false, '2018-06-22 14:44:58.03+00', '2018-06-22 14:44:58.03+00');


--
-- TOC entry 2933 (class 0 OID 16865)
-- Dependencies: 204
-- Data for Name: income; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2926 (class 0 OID 16824)
-- Dependencies: 197
-- Data for Name: knex_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.knex_migrations (id, name, batch, migration_time) VALUES (1, '20180425155002_create_person.js', 1, '2018-06-22 14:27:28.089+00');
INSERT INTO public.knex_migrations (id, name, batch, migration_time) VALUES (2, '20180622102549_create_debts.js', 1, '2018-06-22 14:27:28.169+00');
INSERT INTO public.knex_migrations (id, name, batch, migration_time) VALUES (3, '20180622102621_create_income.js', 1, '2018-06-22 14:27:28.245+00');
INSERT INTO public.knex_migrations (id, name, batch, migration_time) VALUES (4, '20180622102625_create_product.js', 1, '2018-06-22 14:27:28.28+00');
INSERT INTO public.knex_migrations (id, name, batch, migration_time) VALUES (5, '20180622102637_create_transaction.js', 1, '2018-06-22 14:27:28.437+00');
INSERT INTO public.knex_migrations (id, name, batch, migration_time) VALUES (6, '20180622112411_create_goods.js', 1, '2018-06-22 14:27:28.537+00');


--
-- TOC entry 2927 (class 0 OID 16830)
-- Dependencies: 198
-- Data for Name: knex_migrations_lock; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.knex_migrations_lock (is_locked) VALUES (0);


--
-- TOC entry 2929 (class 0 OID 16835)
-- Dependencies: 200
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.person (id, cpf, name, address, created_at, updated_at) VALUES (2, '87368663260', 'Jhon Doe', 'Avenida Amazonas, 100, Centro, Belo Horizonte, Minas Gerais', '2018-06-22 14:29:47.172+00', '2018-06-22 14:29:47.172+00');
INSERT INTO public.person (id, cpf, name, address, created_at, updated_at) VALUES (1, '07368680629', 'Lucas Simon Rodrigues Magalhaes', 'Rua Manoel Sabino Nogueira, 164, Palmares, Belo Horizonte, Minas Gerais', '2018-06-22 14:28:43.735+00', '2018-06-22 14:28:43.735+00');


--
-- TOC entry 2935 (class 0 OID 16879)
-- Dependencies: 206
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.product (id, name, created_at, updated_at) VALUES (1, 'Moto Yamaha Fazer 250 17/17', '2018-06-22 14:28:25.421+00', '2018-06-22 14:28:25.421+00');
INSERT INTO public.product (id, name, created_at, updated_at) VALUES (2, 'Palio 1.0 Kit Celebration 13/13', '2018-06-22 14:28:31.85+00', '2018-06-22 14:28:31.85+00');
INSERT INTO public.product (id, name, created_at, updated_at) VALUES (3, 'Mochila Targus SKU 1268421578874-545488741', '2018-06-22 14:28:38.683+00', '2018-06-22 14:28:38.683+00');
INSERT INTO public.product (id, name, created_at, updated_at) VALUES (4, 'Notebook Dell 4470 Core I7 8GB de RAM', '2018-06-22 14:44:21.485+00', '2018-06-22 14:44:21.485+00');


--
-- TOC entry 2937 (class 0 OID 16887)
-- Dependencies: 208
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.transaction (id, person_id, name, value, total, payment_form, payment_installment, created_at, updated_at) VALUES (1, 1, 'Pagamento de combustivel Posto Ipiranga', 2000, 2000, 'Cartao de credito Visa 1234 1234 1234 1234', 1, '2018-06-22 15:03:28.514+00', '2018-06-22 15:03:28.514+00');
INSERT INTO public.transaction (id, person_id, name, value, total, payment_form, payment_installment, created_at, updated_at) VALUES (2, 1, 'Compra de alimento no Quisque Empadas', 599, 599, 'Cartao de credito Visa 1234 1234 1234 1234', 1, '2018-06-22 15:04:29.546+00', '2018-06-22 15:04:29.546+00');
INSERT INTO public.transaction (id, person_id, name, value, total, payment_form, payment_installment, created_at, updated_at) VALUES (3, 1, 'Compra de vestuario na Hering', 7239, 14478, 'Cartao de credito Visa 1234 1234 1234 1234', 2, '2018-06-22 15:06:19.111+00', '2018-06-22 15:06:19.111+00');


--
-- TOC entry 2956 (class 0 OID 0)
-- Dependencies: 201
-- Name: debt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.debt_id_seq', 1, false);


--
-- TOC entry 2957 (class 0 OID 0)
-- Dependencies: 209
-- Name: good_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.good_id_seq', 2, true);


--
-- TOC entry 2958 (class 0 OID 0)
-- Dependencies: 203
-- Name: income_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.income_id_seq', 1, false);


--
-- TOC entry 2959 (class 0 OID 0)
-- Dependencies: 196
-- Name: knex_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.knex_migrations_id_seq', 6, true);


--
-- TOC entry 2960 (class 0 OID 0)
-- Dependencies: 199
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_id_seq', 2, true);


--
-- TOC entry 2961 (class 0 OID 0)
-- Dependencies: 205
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_id_seq', 4, true);


--
-- TOC entry 2962 (class 0 OID 0)
-- Dependencies: 207
-- Name: transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transaction_id_seq', 3, true);


--
-- TOC entry 2786 (class 2606 OID 16856)
-- Name: debt debt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.debt
    ADD CONSTRAINT debt_pkey PRIMARY KEY (id);


--
-- TOC entry 2797 (class 2606 OID 16910)
-- Name: good good_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.good
    ADD CONSTRAINT good_pkey PRIMARY KEY (id);


--
-- TOC entry 2789 (class 2606 OID 16870)
-- Name: income income_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.income
    ADD CONSTRAINT income_pkey PRIMARY KEY (id);


--
-- TOC entry 2777 (class 2606 OID 16829)
-- Name: knex_migrations knex_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations
    ADD CONSTRAINT knex_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2779 (class 2606 OID 16847)
-- Name: person person_address_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_address_unique UNIQUE (address);


--
-- TOC entry 2781 (class 2606 OID 16845)
-- Name: person person_cpf_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_cpf_unique UNIQUE (cpf);


--
-- TOC entry 2783 (class 2606 OID 16843)
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- TOC entry 2791 (class 2606 OID 16884)
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- TOC entry 2794 (class 2606 OID 16895)
-- Name: transaction transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- TOC entry 2784 (class 1259 OID 16862)
-- Name: debt_person_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX debt_person_id_index ON public.debt USING btree (person_id);


--
-- TOC entry 2795 (class 1259 OID 16916)
-- Name: good_person_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX good_person_id_index ON public.good USING btree (person_id);


--
-- TOC entry 2798 (class 1259 OID 16922)
-- Name: good_product_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX good_product_id_index ON public.good USING btree (product_id);


--
-- TOC entry 2787 (class 1259 OID 16876)
-- Name: income_person_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX income_person_id_index ON public.income USING btree (person_id);


--
-- TOC entry 2792 (class 1259 OID 16901)
-- Name: transaction_person_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX transaction_person_id_index ON public.transaction USING btree (person_id);


--
-- TOC entry 2799 (class 2606 OID 16857)
-- Name: debt debt_person_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.debt
    ADD CONSTRAINT debt_person_id_foreign FOREIGN KEY (person_id) REFERENCES public.person(id) ON DELETE CASCADE;


--
-- TOC entry 2802 (class 2606 OID 16911)
-- Name: good good_person_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.good
    ADD CONSTRAINT good_person_id_foreign FOREIGN KEY (person_id) REFERENCES public.person(id) ON DELETE CASCADE;


--
-- TOC entry 2803 (class 2606 OID 16917)
-- Name: good good_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.good
    ADD CONSTRAINT good_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- TOC entry 2800 (class 2606 OID 16871)
-- Name: income income_person_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.income
    ADD CONSTRAINT income_person_id_foreign FOREIGN KEY (person_id) REFERENCES public.person(id) ON DELETE CASCADE;


--
-- TOC entry 2801 (class 2606 OID 16896)
-- Name: transaction transaction_person_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_person_id_foreign FOREIGN KEY (person_id) REFERENCES public.person(id) ON DELETE CASCADE;


--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-06-22 12:08:07 -03

--
-- PostgreSQL database dump complete
--

