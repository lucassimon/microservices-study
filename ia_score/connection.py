# -*- coding: utf-8 -*-

# Python
from json import dumps

# Third
from pika import BlockingConnection, URLParameters


def send_message_amqp(uri, queque, message):

    if not uri:
        raise ValueError('Verifique a configuração do sistema de filas.')

    if not isinstance(uri, str):
        raise ValueError('Verifique a uri do sistema de filas')

    params = URLParameters(uri)
    params.socket_timeout = 5

    if not isinstance(message, dict):
        raise ValueError("A mensagem precisa ser um dicionário")

    try:
        body = dumps(message)
    except Exception as e:
        raise e

    try:
        connection = BlockingConnection(params)
        channel = connection.channel()
        channel.queue_declare(queue=queque, durable=True)
        channel.basic_publish(
            exchange='', routing_key=queque, body=body
        )
        connection.close()

        return True
    except Exception as e:
        raise e
