# -*- coding: utf-8 -*-

# Python
from datetime import datetime

# Apps
from utils import process_english_format_date


QUEQUE = 'microservices.{}'


def create_message(origin, data):

    return {
        "origin": origin,
        "published_at": process_english_format_date(
            datetime.now(), True
        ),
        "context": data
    }
