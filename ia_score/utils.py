# -*- coding: utf-8 -*-

# Python
from datetime import datetime


def process_english_format_date(value, timestamp=False):
    '''
    Returns yyyy-mm-dd format or a timestamp
    '''

    if not isinstance(value, datetime):
        raise ValueError('Aceita somente o tipo datetime')

    if timestamp:
        return value.timestamp()

    return value.strftime("%Y-%m-%d")
