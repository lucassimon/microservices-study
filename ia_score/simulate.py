# -*- coding: utf-8 -*-

# Python
from os import getenv
from os.path import dirname, isfile, join
from random import randrange
# Third
from dotenv import load_dotenv

# Local
from connection import send_message_amqp
from message import QUEQUE, create_message


_ENV_FILE = join(dirname(__file__), '.env')


def get_score(cpf):
    '''
    Dado um cpf realizar o algoritmo I.A e retornar um score
    '''
    return randrange(0, 101)


if __name__ == "__main__":

    if isfile(_ENV_FILE):
        load_dotenv(dotenv_path=_ENV_FILE)

    amqp_uri = getenv('AMQP_HOST')

    origin = 'IA-123'
    # TODO: simulate an action with cpf parameter
    cpf = "07368680629"

    # TODO: Get from service_b or base_b from cpf
    person_id = "object id"

    # TODO: Get from person model
    name = "Lucas Simon Rodrigues Magalhães"

    # IA algorithm
    score = get_score(cpf)

    # TODO: is it really necessary?
    user_id = 1

    queue = QUEQUE.format(origin)

    data = {
        "cpf": cpf, "name": name, "score": score,
        "person_id": person_id, "user_id": user_id
    }

    try:
        message = create_message(origin, data)
        send_message_amqp(amqp_uri, queue, message)

    except Exception as e:
        print(e)
