# Problema

Elabore uma solução que ofereça armazenamento, processamento e disponibilização desses dados.

## Entendimento e aplicação dos micro serviços

Abaixo estão separados as funcionalidades, bases de dados e escopo do projeto.

### Base A

A primeira delas, que chamamos de Base A, é extremamente sensível e deve ser protegida com os maiores níveis de segurança, mas o acesso a esses dados não precisa ser tão performática.

- CPF
- Nome
- Endereço
- Lista de dívidas

### Base B

Também possui dados críticos, mas ao contrário da Base A, o acesso
precisa ser um pouco mais rápido. Uma outra característica da Base B é que além de consultas ela é utilizada para extração de dados por meio de algoritmos de aprendizado de máquina.

O segundo, acessa a Base B que contém dados para cálculo do Score de Crédito. O Score de Crédito é um `rating` utilizado por instituições de crédito (bancos, imobiliárias, etc) quando
precisam analisar o risco envolvido em uma operação de crédito a uma entidade.

- Idade
- Lista de bens (Imóveis, etc)
- Endereço
- Fonte de renda


### Base C

Que não possui nenhum tipo de dado crítico, mas precisa de um acesso
extremamente rápido.

O último serviço, acessa a Base C e tem como principal funcionalidade, rastrear eventos relacionados a um determinado CPF.

- Última consulta do CPF em um Bureau de crédito (outros).

- Movimentação financeira nesse CPF.

- Dados relacionados a última compra com cartão de crédito vinculado ao CPF.


## Desafio 1

Como você resolveria esse problema? Divague sobre os seguintes tópicos e outros que ache adequado, sinta-se a vontade para desenhar, escrever, criar diagramas, vídeos, apresentação, ou qualquer outro meio que facilite o entendimento por parte dos avaliadores:

- Tecnologias adotadas
- Arquitetura utilizada
- Dados armazenados (já listados ou que você acrescentaria)

### Sistema A

Análise e arquitetura do `Sistema A`

#### Banco de Dados

Devido as necessidades de sensibilidade dos dados contidos nessa base de dados acredito que uma solução baseada em Oracle/Postgres é  ideal para suprir essa demanda. Estes dois bancos são poderosos e possuem a estabilidade necessária assim como robustez de armazenamento. O Oracle é um banco pago e seu custo de implementação e manutenção são altos. Vale ressaltar o alto custo de um especialista neste tipo de banco.

O Postgres é um banco de dados open source e possui uma robustez incrível e se adequa muito bem desde a quantidade de dados, como em performance.

Ambos os casos deve haver um profissional especializado, DBA, para executar, implantar, performar e dar manutenção no banco de dados.

Outro fator nesta implementação é a disponibilidade dos servidores. Eles devem estar bem alocados em um DataCenter de alta disponibilidade e com técnicas de redundância entre discos. Um bom DataCenter possui segurança interna e monitorada 24/7, rotinas de backup, refrigeração, suporte e muito mais. Assim suprimos parte do quesito segurança.

#### Modelagem

![BaseA](base_a.png)

Adicionei a fonte de renda aqui pois é um dado sigiloso, assim como as dívidas. A tabela `transactions` é utilizada para inserir informações de compra do consumidor e também é um dado sigiloso.

Fontes de renda `(income)` foi adicionada a este sistema  por ser um dado sigiloso, assim como fontes de renda `(goods)`. Ambos tratam de informações necessárias para receita federal e são extremamente sigilosas e inalteráveis.

Por causa dessas tabelas, valida ainda mais a utilização de Banco de dados relacional atômico que suporta transações, como o Oracle ou Postgres.

Pensando sobre isso se for avaliarmos tecnologias, podemos pensar em um banco de dados imutável para essas transações e dados de consumo e receita. A Nubank utiliza uma tecnologia proprietária para isso.


#### Configuração do servidor web.

Para prover essa arquitetura creio que o Banco de dados deve ser disponibilizado em uma rede privada. Ou seja separada do servidor do aplicativo em uma faixa de IP que somente esse serviço possa enxergar. Assim não expomos o banco de dados para a nuvem.

Outra configuração de suma importância é a configuração de uma VPN para que os dados trafegados e requisições possam ser criptografadas evitando qualquer tentativa de invasão. Essa VPN será utilizada tanto para administradores de banco de dados, quanto para manutenção do servidor do aplicativo.

Sobre o servidor do aplicativo, deve configurar o firewall corretamente e expor somente a porta do aplicativo e o acesso ao servidor(es) de banco de dados.

Este aplicativo por ter a natureza de somente ser consumido, traduzindo operações GET, pode-se utilizar o conceito de filas e seu respectivo gerenciador (Rabbit MQ) que também deve ser configurado corretamente em um novo servidor com todas as regras de rede privada e firewall.


#### A aplicação

Para facilitar o desenvolvimento da aplicação vou abstrair esse conceito de filas e adicionar o Node.Js construindo uma aplicação assíncrona exposta no protocolo HTTP.

Por que Node.JS? É assíncrono por definição e natureza. Suas requisições não são bloqueantes. Ou seja o acesso ao banco de dados tende a não bloquear demais requisições. De acordo com o contexto do desafio: "o acesso a esses dados não precisa ser tão performática", a espera pela resposta não é um requisito funcional portanto o retorno será somente enviado para a aplicação gateway quando estiver concluído.

#### Configurações

Primeiro é importante configurar um Postgres na maquina de desenvolvimento. Para facilitar utilizei o Docker.

```shell
$ docker run --name postgres -p 5432:5432 -d postgres
```

Entre no postgres via linha de comando e crie um banco de dados chamado humans.

```shell
$ psql -h localhost -U postgres
postgres=# create database humans;
```

É possível executar migrations conforme o comando `$ npm run knex "migrate:latest"` ou importar um backup que esta localizado no diretorio `./scripts/base_a.sql`


Antes de executar o projeto deve exportar a variavel de ambiente `NODE_ENV`

```shell
$ export NODE_ENV='dev'
```

Em seguida `$ npm run dev`. O resultado deve se parecer com isso:

```shell
$ npm run dev

> service_a@1.0.0 dev /workspaces/python/micro-servicos-python/service_a
> nodemon server.js

[nodemon] 1.17.5
[nodemon] to restart at any time, enter `rs`
[nodemon] watching: *.*
[nodemon] starting `node server.js`
Server running at: http://localhost:8000
```

### Sistema B

Este é um sistema que precisa ter uma boa disponibilidade de dados e que seja tolerante a falhas. Apesar de mais a frente explicarmos a API Gateway como aplicativo principal, acredito que esse `sistema B` será de extrema importância e requer conceitos de disponibilidade, tolerância a falhas, rapidez e robustos.

Caso eu tivesse conhecimento em Elixir e Erlang com certeza implementaria algo em cima dessa tecnologia por ser a especialidade dela.

Porém para a entrega e por não conhecer tão bem o ecossistema do Erlang, processos, supervisores, paradigma funcional vou optar por uma tecnologia mais simples com a utilização do Python que será abordado mais adiante.


#### Banco de Dados

O requisito de, leitura e acesso a algoritmos de inteligência artificial complica sobre a decisão do banco de dados. Outro ponto a considerar nessa escolha e o grande volume de dados por se tratar de um `Big Data` `(data lake)` e também quanto acesso por empresas que poderão consumir esse serviço.

É um caso a se estudar e testar qual é o adequado para este caso. Fico na dúvida em Elastic Search, MongoDB, Oracle, Postgres.

A escolha fica com o MongoDB. No melhor caso seria o ElasticSearch mas não o conheço profundamente para aplicar em um projeto `B.I` e sua tecnologia por de trás das buscas. Já o MongoDB devido a performance em leitura, disponibilidade de criar réplicas do banco, também é `schemaless` sem ficar presos a padrões de bancos relacionais, inserindo assim, dados provenientes de uma pesquisa ao `Sistema A` como o nome, id de referência, endereço.

O endereço, descrito anteriormente, é importante para uma análise preditiva. Por exemplo o local onde a pessoa trabalha ou mora pode influenciar diretamente na pontuação do algoritmo fazendo uma correlação entre pessoas, capacidade de renda, vizinhança e etc...

Este `System B` consulta dados das pessoas através do `System A` e salva os dados caso não exista na base. Por se tratar de um `B.I` os dados serão repetidos e precisam estar em conformidade com os dados da `base A`.

Esse pode ser um problema ao repetir os dados. Explicando de uma forma mais simples, todas as mudanças executadas na `base A` de determinados campos e eventos devem atualizar os dados dessa `base B`. Podemos fazer com que os campos necessários para processamento sempre busque da `base A` porém pode ocorrer um congestionamento e performance.

Novamente estudar as tecnologias e modo como construir essa aplicação a qual considero a mais importante dentro do ecossistema.


#### Modelagem

![BaseB](base_b.png)

Devido a finalidade de ser um `data lake` toda e qualquer tipo de input de dados é útil. Por isso além de repetir os dados do `System A` adicionei novas coleções que podem ser úteis como cartões de crédito, cidades, empresas, produtos. Todo input de dados é bem vindo.

Claro que precisa de uma boa organização e uma analise detalhada do que pode ser útil para a I.A. Apesar de atualmente o fator espaço (bytes) não é mais um problema.


#### A aplicação

Este serviço é valido implementar em Python com o framework Flask. Pois tem uma facilidade em comunicar com o MongoDB com o pacote MongoEngine e também é muito fácil escrever os métodos com ele ao contrário do Django. O Django apesar de ter o pacote Django Rest Framework é muito verboso codificar em cima das CBGV e dos utilitários que o pacote fornece, as vezes, ficamos perdidos com tanta coisa. O Django também, ao meu ver, é uma ótima ferramenta para prototipação de sistemas pois conseguimos desenvolver rapidamente uma aplicação conectando com banco de dados, exceto MongoDB, um painel administrativo funcional e módulos para autenticação. Porém quando você precisa de algo mais robusto, simples de ser feito Flask é muito bom.

Outro motivo para a escolha do Python é seu uso para I.A. Apesar de não conhecer muito sobre o tema, o Python é amplamente utilizado para desenvolver esses algoritmos e possui boas bibliotecas para análise de dados como numpy, pandas dentre outras.

Logo um sistema homogêneo, apesar de estarmos trabalhando com micro serviços (esta pode ter diferentes linguagens), pode ser interessante para a equipe que está a frente desta aplicação ser o mais homogêneo possível.

Qualquer operação a essa base deve gerar um evento para uma fila, RabbitMQ, para popular os `Sistema B` e `Sistema C`. Haverá múltiplos consumidores para realizar essa operação computacional.

#### Configurações


Primeiro é importante configurar o MongoDB na maquina de desenvolvimento. Para facilitar utilizei o Docker.

```shell
$ docker run --name mongo-latest -p 27017:27017 -d mongo
```

Criar um virtualenv com `python3`

```shell
$ mkvirtualenv -p /usr/bin/python3 microservices
```

Configure as variáveis de ambiente e altere se sentir necessário. Por exemplo o MONGODB_URI está previamente configurado para `mongodb://localhost:27017/service_b`.

```shell
$ cp .env-example .env
```

Instale os requerimentos da aplicação:

```
$ pip3 install -r requirements/dev.txt
```

Instale o aplicativo local para execução de testes:

```
$ pip install -e .
```

Com, isso você pode executar os comandos disponíveis:

```shell
$ make
make clean
       prepare development environment, use only once
make clean-build
       Clear all build directories
make isort
       run isort command cli in development features
make lint
       run lint
make test
       run tests
make run
       run the web application
```

O ideal para levantar um servidor de desenvolvimento é o `$ make run`. Ele irá executar uma limpeza em arquivos `.pyc e outros`, executar o lint, executar os testes da aplicação e por fim executar a app.

Execute o comando `flask adds_sample_data` para adicionar alguns registros simples na base de dados.


### IA-123 produtor

Criei uma simulação simples para enviar mensagens na fila através do diretório `ia-score`. Os dados estão fixos mas está enviando mensagens a um rabbit-mq.

#### Configurações

Primeiro é importante configurar o MongoDB na maquina de desenvolvimento. Para facilitar utilizei o Docker.

```shell
$ docker run -d --log-driver=syslog -e RABBITMQ_DEFAULT_USER=root -e RABBITMQ_DEFAULT_PASS=root -e RABBITMQ_NODENAME=my-rabbit --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management
```

Criar um virtualenv com `python3`

```shell
$ mkvirtualenv -p /usr/bin/python3 ia
```

Configure as variáveis de ambiente e altere se sentir necessário. `AMQP_HOST="amqp://root:root@localhost"`.

```shell
$ cp .env-example .env
```

Execute o script python: `$ python simulate.py`. Esse comando irá enviar somente 1 mensagem. Ha muito o que melhorar nesse script

### IA-123 consumidor

Todo produtor precisa de consumidor. Seguindo essa regra o diretório `ia-consumer` possui scripts feitos com Node.JS para consumir a fila do RabbitMQ e salvar os dados no banco de dados MongoDB para a `base_c`.

#### Configurações

Faça um `$ source .env-example` para exportar as variaveis de ambiente. Se for necessário altere os arquivos para adequar ao seu teste.

Instale os pacotes necessários `$ npm install`.

Execute o consumidor: `$ npm start`.

O resultado deve-se paracer com output:

```shell
$ npm start

> ia_consumer@1.0.0 start /workspaces/python/micro-servicos-python/ia_consumer
> node index

{ cpf: '07368680629',
  name: 'Lucas Simon Rodrigues Magalhães',
  score: 75,
  person_id: 'object id',
  user_id: 1 }
 [x] Received
{ cpf: '07368680629',
  name: 'Lucas Simon Rodrigues Magalhães',
  score: 14,
  person_id: 'object id',
  user_id: 1 }
 [x] Received
```


### Sistema C

Este sistema como prioridade deve ser rápido. Falou em rapidez pensei em GOLang. Porém a aplicação de desenvolvimento será feito em Python ou NodeJs.

Coloquei GOLang pois GO é muito rápido. Tive a experiência de ver o GO em ação ao consumir API's de companhias aéreas. O contexto era que para vender uma passagem aérea é necessário consumir uma API feita em .NET e longos arquivos em XML. Entretanto, eram várias requisições para compor uma compra:

- uma requisição para verificar voos disponíveis

- uma requisição para verificar disponibilidade (x2)

- uma requisição para tarifar o voo

- uma requisição para reservar

- uma requisição para marcar o assento do(s) passageiro(s).

Todas essas requisições o XML era extenso e o Python apesar de ter diversas bibliotecas para iteração com esse tipo arquivo se tornou lento, pois tinha que fazer a consulta, manipular os dados, montar um json corretamente com as informações e retornar para o usuário. O exemplo anterior foi de 01 companhia aérea mas o Brasil opera com 4 grandes empresas.

Logo, ao implementar uma API em GOLang a resposta para a pesquisa de voos disponíveis era muito rápida. Realmente muito rápida, infelizmente não tenho números ou como fornecer exemplos reais.

Assim como o `Sistema B` o ElasticSearch é ideal para pesquisa. Ainda mais pelo tipo de necessidade que a arquitetura apresenta e também precisa receber um input de dados das ações do sistema e usuários como também utilizá-lo para gerar log's de erros e consumo de cada serviço.


#### Banco de Dados

![Base C](base_c.png)

Devido a falta de conhecimento da tecnologia ElasticSearch, opto pelo MongoDB pelos motivos descritos no `Sistema B`.


#### Função Consulta CPF

> Última consulta do CPF em um Bureau de crédito.

Nosso input de dados será o `IA-123 produtor e consumidor` ao realizar o score de crédito pelas empresas.

#### Configurações

Foi implementado somente esta função com o MicroJS.

Instale os pacotes do diretório `service_c1` executando `npm install`.

Exporte a variável de ambiente `$ export DATABASE_URL="mongodb://localhost:27017/service_c"`

Feito isso, execute o comando para levantar o servidor: `$ npm run dev`.

Com isso podemos testar `$ http -v POST 0.0.0.0:3000 cpf="07368680629"`

Resultando em

```shell
> #9 POST /
{
  cpf: "07368680629"
}

< #9 200 [+4ms]
{
  _id: "5b2da937a15f9665934678c6",
  origin: "IA-123",
  published_at: "1529719095.622415",
  person_id: "object id",
  user_id: "1",
  name: "Lucas Simon Rodrigues Magalhães",
  cpf: "07368680629",
  score: 14,
  createdAt: "2018-06-23T01:58:15.633Z",
  updatedAt: "2018-06-23T01:58:15.633Z",
  __v: 0
}

```
Este micro serviço ou função será utilizada pelo GraphQL logo abaixo.

#### Função Consulta Movimentação Financeira

> Movimentação financeira nesse CPF.

Nosso input de dados será o `Sistema A` ao cadastrar as transações efetuadas pela pessoa física. Cada transação gera um evento via RabbitMQ para o ElasticSearch. Esse mesmo evento será utilizado no próximo evento.


#### Função Consulta Ultima compra

> Dados relacionados a última compra com cartão de crédito vinculado ao CPF.

Utiliza a mesma descrição da função anterior.


##### App

Acho que o Node.JS atenderia bem. Inclusive existe um pacote `serverless` que faz deploy automático da aplicação na Amazon.

Entretanto, um Hapi.JS ou MicroJS pode resolver o problema já que é somente uma função `GET`


## Desafio 2

> Será necessário também desenvolver um meio pelo qual esses dados
estarão disponíveis. É interessante imaginar os possíveis interessados em consumir esses dados para que uma única solução possa ser construída de modo a atender o máximo de situações possíveis.

Vejo que os principais utilizadores para consumo dessa API Gateway serão os Bancos, imobiliarias, financeiras, concessionária e demais instituições que forneçam altos valores em crédito.

Outros atores que inserem dados são instituições governamentais, policia, receita federal, empresas de bens e consumo, cartões de crédito. Por exemplo, dados de ficha criminal são inseridos pela policia atrelado ao CPF, assim como o não pagamento de um curso de faculdade é considerado um input de dados e deve ser enviado ao sistema ou a compra de um automóvel considerando um bem, até um simples pão de queijo pode ser computado como bem de consumo gerando uma transação. Creio que toda compra é rastreada e esses dados podem ser utilizados para a I.A.

Por se tratar de uma API Gateway ela pode ser exposta como um serviço web e podendo ser consumido por diversos clientes como: aplicativos web, mobile, desktop, IoT e etc...

Vale lembrar que essa API Gateway deve conter autenticação e métodos restritos para cada operação em nível de perfil. Toda a ação deve gerar um registro em uma base de logs.

No caso de acessos restritos, por exemplo, uma funcionária de um banco pode pesquisar o Score de um crédito de um determinado cpf ou até mesmo de um cnpj. Porém se ela precisar de uma avaliação detalhada ela não pode simplesmente verificar o histórico de bens do cpf. Para isso ela deve escalonar esse processo de análise para um gerente e este executar avaliação com os devidos dados em mãos e então retornar para a funcionária a aprovação de crédito ou não.

Dentre os principais métodos posso listar aqui:

- Score de Crédito, por cpf. Retornando a pontuação e dados da pessoa física.

- Buscar dados do cpf. Retorno dos dados da pessoa física.

- Lista de bens, por cpf, páginado. Acesso com perfil mais restrito.

- Lista de dividas, por cpf, páginado. Acesso com perfil mais restrito.

- Lista de receitas, por cpf, páginado. Acesso com perfil mais restrito.

- Salva dados de transações

- Salva dados de bens

- Salva dados de débitos

- Salva dados de fontes de renda

### Tecnologias.

Atualmente surgiu o GraphQL como uma alternativa aos padrões REST no protocolo HTTP.

Um dos benefícios do GraphQL são:

- Facilidade de gerar consultas a partir de uma única requisição.

- Ha também a possibilidade de buscar somente os dados necessários.

Ao contrário do modelo REST que o retorno do JSON é fixo, com o GraphQL especificamos os dados que queremos na consulta. Esse quesito é importante pois imaginando em uma aplicação mobile onde a conexão de banda larga é pequena os dados transmitidos serão muito menores do que um extenso JSON. Ganha-se em rápidez e economia.

- Obter dados relacionados de maneira fácil.

Independente se estão na mesma base de dados ou em outros serviços. Em caso de serviços basta fazer uma chamada para API ou SOAP ou RPC no desenvolvimento deste `resolve`.

Além dos benefícios citados acima, o GraphQL, não está preso a uma tecnologia ou linguagem de programação. Ele é uma metodologia de desenvolvimento que pode ser empregada sobre Python, GoLang, NodeJS e demais linguagens existentes no mercado.

### Outras alternativas

Existe a opção de criar uma API REST para fazer o apontamento para estes serviços, passando o nome do serviço, o método e o endpoint a ser consumido. Ficando assim:

```json
{
    "name": "Get person detail",
    "method": "GET",
    "base_url": "http://localhost:8000",
    "endpoint": "/persons/07368680629"
}
```

Ao receber este input de dados a API trata de manipular os dados e montar uma requisição para o referido pedido. Essa é uma solução, mas internamente creio que não é tão elegante. Deverá ser escrito mais linhas de código e fazer condicionais a todo momento. Também o JSON retornado não será customizado como o GraphQL.

### Configurações

Existem diversas formas como criar uma api gateway, como por exemplo o `apollo server`, `graphene` para `Python`. Neste projeto utilizei a versão do `graphqljs` e `hapijs`.

Acesse o diretório `gateway` e instale os pacotes `npm install`. Em seguida execute `node server.js`.

Muitas `queries` estão com valores fixos, exceto `lastEventScoreByCpf(cpf:"07368680629")` que comunica com o `service_c1`. Portanto acesse no seu navegador a url `http://localhost:7000/graphiql` e coloque a query abaixo:

```
{
  lastEventScoreByCpf(cpf:"07368680629") {
    id,
    name,
    score,
    person_id
  }

}
```

Em seguida aperte o `play` para executar a query. Com isso podemos mostrar o poder do `graphql` para acessar um serviço. Lembrando que, o acesso a serviços pode ser feita através de diversos protocolos, nesse caso utilizamos o `http`.


## Visão geral e problemas encontrados

![Overview](overview.png)


### Autênticação

Quando penso em arquitetura de micro serviços sempre esbarro no quesito autenticação e segurança respectivamente. Ou seja cada serviço deve um acesso e credenciais que informam o nível de privilégios.

Pensei em ter um serviço separado somente para criar usuários e perfis. Ficando responsável somente por fazer uma autênticação, validando password, email, chave de segurança? Em seguida retornar um JWT com a informação do usuário e seu nível de privilégio com data de expiração bem baixa.

A API Gateway ficaria responsável por passar esse JWT para as aplicações e cada APP validaria o JWT, válido ou não, expirado ou não.

Logo, tudo estiver OK, a resposta é retornada para a API Gateway conforme solicitada para posteriormente ser consumida por qualquer tipo de `client` (webapp, mobile, terminal, http...).


### Escalabilidade

Utilização de docker com kubernates é de grande valia para esse tipo de arquitetura. Porém apresenta mais uma nova necessidade dentro do time para gerenciar as maquinas que devem ser levantas quando houver necessidade. Ou seja, necessita-se de uma inteligência e orquestração de toda essa infraestrutura tornando-a complexa.

### Técnicas de deploy

Todos os serviços devem passar por uma ferramenta de testes e integração continua. Cada serviço deve ser independente não tendo muito acoplamento e trabalhar sempre com `ids`.

Pelo pouco que estudei existe a técnica Blue Green deploy com as ferramentas de testes, gerenciamento de configuração e etc...

Ter uma forma como voltar a versão do commit em caso de problemas de forma rápida e com o minimo impacto. Melhor ainda se não tiver danos.

### Monitoramento

Todos os serviços precisam ser monitorados. Para isso existe ferramentas como o sentry, serviço de logs, serviço como NewRelic. Para qualquer falha gerar um alerta para os administradores do parque e responsáveis.


### Backup

Backup é segurança. Além do git como repositório e gerenciador de versões é preciso ter um backup da aplicação e do banco de dados.


